﻿using Microsoft.AspNetCore.Mvc;
using ZadanieApis;
using ZadanieApis.Models;

public class AdminController : Controller
{
    private MyDbContext db = new MyDbContext();

    public ActionResult Admin()
    {
        List<Orders> orders = db.Orders.ToList();
        return View(orders ?? new List<Orders>());
    }

    [HttpPost]
    public ActionResult UpdateStatus(int orderId, string newStatus)
    {
        var order = db.Orders.Find(orderId);

        if (order != null)
        {
            order.Status = newStatus;

            if (newStatus == "Received")
            {
                db.Orders.Remove(order);
            }

            db.SaveChanges();
        }

        return Json(new { success = true });
    }
    public ActionResult GetLatestOrders()
    {
        List<Orders> latestOrders = db.Orders.ToList();
        return Json(latestOrders);

    }
}
