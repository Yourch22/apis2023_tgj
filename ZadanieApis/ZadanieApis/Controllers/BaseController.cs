﻿namespace ZadanieApis.Controllers
{
    using Microsoft.AspNetCore.Http;
    using Microsoft.AspNetCore.Mvc;
    using ZadanieApis.Models;

    public class BaseController : Controller
    {
        protected const string UserIdCookieKey = "UserId";
        protected const string UsernameCookieKey = "Username";
        protected const string UserCartCookieKey = "UserCart";

        protected void SetUserCookies(User user, string userCart)
        {
            Response.Cookies.Append(UserIdCookieKey, user.UserID.ToString());
            Response.Cookies.Append(UsernameCookieKey, user.Username);
            Response.Cookies.Append(UserCartCookieKey, userCart);
        }

        protected int? GetUserIdFromCookies()
        {
            if (Request.Cookies.TryGetValue(UserIdCookieKey, out string userIdStr))
            {
                if (int.TryParse(userIdStr, out int userId))
                {
                    return userId;
                }
            }

            return null;
        }

        protected string GetUsernameFromCookies()
        {
            return Request.Cookies[UsernameCookieKey];
        }

        protected string GetUserCartFromCookies()
        {
            return Request.Cookies[UserCartCookieKey];
        }

        protected void ResetCart()
        {
            Response.Cookies.Append(UserCartCookieKey, "");
        }

        protected void Logout()
        {
            Response.Cookies.Delete(UserIdCookieKey);
            Response.Cookies.Delete(UsernameCookieKey);
            Response.Cookies.Delete(UserCartCookieKey);
        }
    }
}