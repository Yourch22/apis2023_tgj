﻿using Microsoft.AspNetCore.Mvc;
using ZadanieApis;
using ZadanieApis.Models;
using System.Linq;
using ZadanieApis.Controllers;
using System.Collections.Generic;

public class HomeController : BaseController
{
    private MyDbContext db = new MyDbContext();

    public ActionResult Index()
    {
        return View();
    }

    [HttpPost]
    public ActionResult Login(string username, string password)
    {
        var user = db.Users.FirstOrDefault(u => u.Username == username && u.Password == password);
        string userCart = "";

        if (user != null)
        {
            SetUserCookies(user, userCart);

            if (user.Role.ToLower() == "admin")
            {
                return RedirectToAction("Admin", "Admin");
            }
            else if (user.Role.ToLower() == "user")
            {
                return RedirectToAction("UserCatalog", "User");
            }
        }

        ViewBag.ErrorMessage = "Invalid username or password.";
        return View("Index");
    }
    public ActionResult Logout()
    {
        base.Logout();
        return RedirectToAction("Index");
    }
}

