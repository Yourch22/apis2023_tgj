﻿using Microsoft.AspNetCore.Mvc;
using System.Data.Entity;
using ZadanieApis.Models;

namespace ZadanieApis.Controllers
{
    public class UserController : BaseController
    {
        private MyDbContext db = new MyDbContext();

        public ActionResult UserCatalog()
        {
            var userId = GetUserIdFromCookies();
            var username = GetUsernameFromCookies();

            if (userId.HasValue && !string.IsNullOrEmpty(username))
            {
                ViewBag.CurrentUserID = userId;
                ViewBag.CurrentUsername = username;

                List<Products> products = db.Products.Take(100).ToList();
                return View(products ?? new List<Products>());
            }

            return RedirectToAction("Index", "Home");
        }

        public async Task<ActionResult> Product(string productId)
        {
            var userId = GetUserIdFromCookies();
            var username = GetUsernameFromCookies();
            string recommendedProducts = await GetResponseFromApi("http://recommendation-system.azurewebsites.net/recommendations?product_id=" + productId);
            recommendedProducts = recommendedProducts.Replace("[", string.Empty);
            recommendedProducts = recommendedProducts.Replace("]", string.Empty);
            recommendedProducts = recommendedProducts.Replace("\"", string.Empty);
            recommendedProducts = recommendedProducts.Replace("\n", string.Empty);
            string[] array = recommendedProducts.Split(',');
            if (userId.HasValue && !string.IsNullOrEmpty(username))
            {
                ViewBag.CurrentUserID = userId;
                ViewBag.CurrentUsername = username;

                Products product = db.Products.Where(e => e.product_uid == productId).First();
                List<Products> productsList = new List<Products> { product };
                foreach (string stringId in array)
                {
                    product = db.Products.Where(e => e.product_uid == stringId).First();
                    productsList.Add(product);
                }
                return View(productsList ?? new List<Products>());
            }
            return RedirectToAction("Index", "Home");
        }

        static async Task<string> GetResponseFromApi(string apiUrl)
        {
            using (HttpClient client = new HttpClient())
            {
                client.Timeout = TimeSpan.FromSeconds(150);
                HttpResponseMessage response = await client.GetAsync(apiUrl);
                if (response.IsSuccessStatusCode)
                {
                    return await response.Content.ReadAsStringAsync();
                }
                else
                {
                    throw new HttpRequestException($"Error: {response.StatusCode} - {response.ReasonPhrase}");
                }
            }
        }

    public ActionResult Cart()
        {
            string userCart = GetUserCartFromCookies();
            if (userCart != "")
            {
                string[] array = userCart.Split(',');
                List<Products> productsList = new List<Products>();
                foreach (string stringId in array)
                {
                    Products product = db.Products.Where(e => e.product_uid == stringId).First();
                    productsList.Add(product);
                }
                return View(productsList ?? new List<Products>());
            }

            return View(new List<Products>());
        }

        public ActionResult AddToCart(string productId, string userId)
        {
            string userCart = GetUserCartFromCookies();
            if (userCart == "")
            {
                userCart = productId;
            }
            else
            {
                userCart = userCart + "," + productId;
            }
            Response.Cookies.Append(UserCartCookieKey, userCart);
            return RedirectToAction("Cart", "User");
        }

        public ActionResult MakeOrder()
        {
            Orders newOrder = new Orders();
            newOrder.OrderID = 0;
            newOrder.UserID = (int)GetUserIdFromCookies();
            newOrder.OrderDate = DateTime.Now;
            newOrder.ProductUIDs = GetUserCartFromCookies();
            newOrder.Status = "Pending";

            db.Orders.Add(newOrder);
            db.SaveChanges();

            ResetCart();
            return RedirectToAction("UserCatalog", "User");
        }

        public ActionResult OrdersHistory()
        {
            var userId = GetUserIdFromCookies();
            var username = GetUsernameFromCookies();

            if (userId != null)
            {
                ViewBag.CurrentUserID = userId;
                ViewBag.CurrentUsername = username;
                List<Orders> userOrders = db.Orders.Where(o => o.UserID == userId).ToList();

                return View("OrdersHistory", userOrders ?? new List<Orders>());
            }

            return RedirectToAction("Index", "Home");
        }
    }
}