﻿using System.Data.Entity;
using ZadanieApis.Models;

namespace ZadanieApis
{
    public class MyDbContext : DbContext
    {
        public MyDbContext() : base("Server=tcp:apis-zadanie-shop.database.windows.net,1433;Initial Catalog=apis-zadanie-shop;Persist Security Info=False;User ID=shop-admin;Password=my-task123465!;MultipleActiveResultSets=False;Encrypt=True;TrustServerCertificate=False;Connection Timeout=30;")
        {
        }

        public DbSet<User> Users { get; set; }
        public DbSet<Orders> Orders { get; set; }
        public DbSet<Products> Products { get; set; }
    }
}