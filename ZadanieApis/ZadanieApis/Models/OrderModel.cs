﻿using System.ComponentModel.DataAnnotations;

namespace ZadanieApis.Models
{
    public class Orders
    {
        [Key]
        public int OrderID { get; set; }
        public int UserID { get; set; }
        public DateTime OrderDate { get; set; }
        public string? ProductUIDs { get; set; } // Nullable string
        public string Status { get; set; }

        public Orders()
        {
            ProductUIDs = string.Empty; // Initialize to an empty string or another default value
        }
    }


}
