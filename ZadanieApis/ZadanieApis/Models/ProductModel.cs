﻿using System.ComponentModel.DataAnnotations;

namespace ZadanieApis.Models
{
    public class Products
    {
        [Key]
        public string product_uid { get; set; }
        public string product_description { get; set; }
    }
}