﻿using System.ComponentModel.DataAnnotations;

namespace ZadanieApis.Models
{

    public class User
    {
        [Key]
        public int UserID { get; set; }

        [Required]
        [StringLength(255)]
        public string Username { get; set; }

        [Required]
        [StringLength(255)]
        public string Password { get; set; }

        [Required]
        [StringLength(255)]
        public string Role { get; set; }
    }


}
