from sklearn.feature_extraction.text import TfidfVectorizer, CountVectorizer
from sklearn.neighbors import NearestNeighbors
from sklearn.cluster import KMeans
from sklearn.metrics import adjusted_rand_score
import pandas as pd
import pyodbc
import os
from flask import Flask
from flask import request
from flask import jsonify
from flask_cors import CORS
import pickle 
from joblib import Parallel, delayed 
import joblib 
# Database credentials
server = 'apis-zadanie-shop.database.windows.net'
database = 'apis-zadanie-shop'
username = 'shop-admin'
password = 'my-task123465!'

# Connection string
connection_string = f'DRIVER={{ODBC Driver 17 for SQL Server}};SERVER={server};DATABASE={database};UID={username};PWD={password}'
app = Flask(__name__)
CORS(app)

def get_cluster_terms(i, order_centroids, terms):
    """
    :param i: Index of the cluster.
    :param order_centroids: Array or list with order centroids.
    :param terms: List of all terms.
    :return: List of terms for the cluster.
    """
    cluster_terms = []
    for ind in order_centroids[i, :10]:
        cluster_terms.append(terms[ind])
    return cluster_terms

def find_similar_products(input_id, cluster_terms, product_data, max_results=5):
    """
    Find products that match the given cluster terms based on their descriptions.
    :param cluster_terms: List of terms that define the cluster.
    :param product_data: DataFrame with product_uid and product_description columns.
    :param max_results: Maximum number of product IDs to return.
    :return: List of product IDs that match the cluster terms.
    """
    matching_products = []
    match_count = 0
    
    for _, row in product_data.iterrows():
        product_description = row['product_description'].lower()
        product_uid = row['product_uid']
        if product_uid != input_id and any(term.lower() in product_description for term in cluster_terms):
            matching_products.append(row['product_uid'])
            if len(matching_products) >= max_results:
                break

    return matching_products

def get_product_description(product_id):
    """
    Get the product description for a given product ID.
    :param product_id: The product ID to search for.
    :return: The product description string or None if not found.
    """
    try:
        with pyodbc.connect(connection_string) as conn:
            query = "SELECT product_description FROM Products WHERE product_uid = ?"
            cursor = conn.cursor()
            cursor.execute(query, (product_id,))
            row = cursor.fetchone()
            if row:
                return row[0]
            else:
                return None
    except Exception as e:
        print(f"An error occurred: {e}")
        return None

# Example usage
@app.route("/recommendations", methods=["GET"])
def show_recommendations():
    product_id = request.args.get('product_id')
    print(product_id)
    # Connect to the database
    try:
        with pyodbc.connect(connection_string) as conn:
            print("Connected to the database successfully")
            # Load the entire 'product_description' table into a pandas DataFrame
            query = "SELECT * FROM Products"
            product_descriptions = pd.read_sql(query, conn)
    except Exception as e:
        print(f"An error occurred: {e}")
    vectorizer = TfidfVectorizer(stop_words='english')
    product_descriptions = product_descriptions.dropna()
    product_descriptions1 = product_descriptions.head(100)

    X1 = vectorizer.fit_transform(product_descriptions1["product_description"])

    true_k = 14
    model = KMeans(n_clusters=true_k, init='k-means++', max_iter=120, n_init=1, random_state=1992)
    model.fit(X1)
    order_centroids = model.cluster_centers_.argsort()[:, ::-1]
    terms = vectorizer.get_feature_names_out()
    
    description = get_product_description(product_id)

    Y = vectorizer.transform([description])
    prediction = model.predict(Y)
    cluster_terms = get_cluster_terms(prediction[0], order_centroids, terms)
    similar_products = find_similar_products(product_id, cluster_terms, product_descriptions)
    return similar_products
    #list_of_folders = os.listdir()
    #app_path = os.getcwd()
    #substring = 'model.pkl'
    #path_to_model = (app_path + '\\' + substring)
    #list_of_folders = [item for item in list_of_folders if substring in item]
    #print(len(list_of_folders))
    #if len(list_of_folders)==0:
        # Save the trained model as a pickle string. 
        #saved_model = pickle.dumps(model) 
        # Save the model as a pickle in a file 
        #joblib.dump(saved_model, 'model.pkl') 
    #if len(list_of_folders)==1:
        #print("Tu som")
        #model = pickle.load('model.pkl')
        #if isinstance(model, KMeans):
            #order_centroids = model.cluster_centers_.argsort()[:, ::-1]
            #print(order_centroids)
            #terms = vectorizer.get_feature_names_out()
            #print(terms)
            #description = get_product_description(product_id)
    
            #Y = vectorizer.transform([description])
            #prediction = model.predict(Y)
        # print_cluster(prediction[0])
           # cluster_terms = get_cluster_terms(prediction[0], order_centroids, terms)
            #similar_products = find_similar_products(cluster_terms, product_descriptions)
# show_recommendations("Water")
# show_recommendations(description)

if __name__ == "__main__":
    app.run()